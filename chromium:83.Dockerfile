FROM alpine:3.12

LABEL maintainer "Raphaël Droz <raphael.droz@gmail.com>"

RUN apk --no-cache add chromium
ENV DEBUG_ADDRESS=0.0.0.0 DEBUG_PORT=9222
RUN /usr/bin/chromium-browser --version 
CMD ["/usr/bin/chromium-browser"]
