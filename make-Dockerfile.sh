#!/bin/bash

# use ./make-Dockerfile.sh  < table.txt
create() {
    cat >| "${PACKAGE%-*}:${VERSION%.*}.Dockerfile" <<EOF
FROM alpine:$DISTRIBUTION

LABEL maintainer "Raphaël Droz <raphael.droz@gmail.com>"

RUN apk --no-cache add $PACKAGE
ENV DEBUG_ADDRESS=0.0.0.0 DEBUG_PORT=9222
RUN $BINARY --version 
CMD ["$BINARY"]
EOF
}

read_from_file() {
    while read PACKAGE DISTRIBUTION VERSION BINARY; do
	[[ -n $PACKAGE ]] && create;
    done
}

read_from_file
